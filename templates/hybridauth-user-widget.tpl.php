    <div id="hybridauth_widget">
      <div id="providers">
        <?php
          if (!empty($providers)) {
            foreach ($providers as $provider) {
              if (!in_array(ucfirst($provider), $connected)) {
          ?>
          <div class="provider floatLeft">
            <img class="icon" rel="<?=$provider?>" src="/<?=$path?>/icons/<?=$provider?>_32.png" title="<?=$provider?>" />
          </div>      
          <?php
              }
            }          
          } else {
            print 'Sorry, no third party services are enabled yet.';
          }
        ?>
        <div class="clearBoth"></div>
      </div>
      <div id="openidid" style="display:none;">
        <table width="100%" border="0">
          <tr> 
          <td align="center"><img id="openidimg" src="/<?=$path?>/examples/widget_authentication/widget/images/loading.gif" /></td>
      
          </tr>  
          <tr> 
          <td align="center"><h3 id="openidm">Please enter your user or blog name</h3></td>
          </tr>  
          <tr>
          <td align="center"><input type="text" name="openidun" id="openidun" /></td>
          </tr>
          <tr>
          <td align="center">
            <input type="submit" value="Login" id="openidbtn" />
      
            <br />
            <small><a href="#" id="backtolist">back</a></small>
          </td>
          </tr>
        </table> 
      </div>
    </div>
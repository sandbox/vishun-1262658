<h1><?php print ucfirst($provider); ?></h1>
<hr />
<div class="user_photo_wrapper floatRight">
  <div class="user_photo">
    <?php print l('<img src="'.str_replace('_normal.', '.', $profile->photoURL).'" height="100" /><br />'.$profile->displayName, $profile->profileURL, array('html'=>true,'attributes'=>array('target'=>'_blank'))); ?>  
  </div>
</div>
<p><i>No settings available yet.</i></p>

<?php 
if (isset($content['friends'])) { 
?>
<h1>Friends</h1>
<hr />
<?php
  foreach ($content['friends'] as $friend) {
?>
  <img src="<?=$friend->profile_image_url?>" />
<?php
  }
}
?>

<?php 
if (isset($content['followers'])) { 
?>
<h1>Followers</h1>
<hr />
<?php
  foreach ($content['followers'] as $follower) {
?>
  <img src="<?=$follower->profile_image_url?>" />
<?php
  }
}
?>

<?php 
if (isset($content)) { 
?>
<h1>Output</h1>
<hr />
<pre>
<?php
  foreach ($content as $out) {
    //print_r($out);
  }
?>
</pre>
<?php
}
?>

<h1>Your <?php print ucfirst($provider); ?> Profile</h1>
<hr />
<pre>
<?php 
  print_r($profile);
?>
</pre>

<?php drupal_set_title('Register'); ?>
<div id="hybridauth_dashboard_wrapper">
  <div id="hybridauth_details_wrapper" class="floatLeft">
    <div id="hybridauth_providers">
      <?php 
        // output the icon widget
        print theme('hybridauth-user-widget', $path, $providers, $connected);
      ?>    
    </div>
    <div id="hybridauth_details">
      <div class="hybridauth_header">
        <h1></h1>
      </div>
      <?php 
        global $user;
        if ($user->uid=='0') { 
          print '<div id="hybridauth-register">';
          print drupal_get_form('user_register');
          print '</div>';
        } else {
          drupal_goto();
          //print '<div id="hybridauth-register" style="height:225px;margin-top:10px;">You\'re already logged in as <b>'.$user->name.'</b>. To connect a third-party service to your site account, please select from the list to the right to get started.</div>';
        }
      ?>
    </div>
  </div>
  <div class="clearBoth"></div>
</div>
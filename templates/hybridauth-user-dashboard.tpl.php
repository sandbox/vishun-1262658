<div id="hybridauth_dashboard_wrapper">
  <div id="hybridauth_details_wrapper" class="floatLeft">
    <div id="hybridauth_details">
      <?php if (!empty($connected)) { ?>
      <div class="hybridauth_header">
        <h1><?php print t('Currently connected via'); ?></h1>
      </div>
      <?php
        foreach ($connected as $provider) {
          global $user; 
          print l('<div class="provider"><div class="icon_wrapper floatLeft"><img class="icon" idp="'.$provider.'" src="/'.$path.'/icons/'.strtolower($provider).'_32.png" title="'.$provider.'" width="32" height="32" /></div><div class="label_wrapper floatLeft"><h2 style="margin-top:5px;margin-left:10px;">'.$provider.'</h2></div><div class="clearBoth"></div></div>', 'user/'.$user->uid.'/authentication/'.$provider, array('html'=>true));
        } 
      ?>
      <div class="clearBoth"></div>
      <ul>
        <li><?php print l('Logout from all services', 'hybridauth/logout'); ?></li>
      </ul>
      <?php
        } else {
      ?>
      <div class="hybridauth_header">
        <h1><?php print t('Not currently connected'); ?></h1>
      </div>
      <?php print t('To connect a third-party service with your account, please select from the right.'); ?>
      <?php
        }
      ?>
    </div>
  </div>
  <div id="hybridauth_providers_wrapper" class="floatLeft">
    <div id="hybridauth_providers">
      <div class="hybridauth_header">
        <h1><?php print t('Available service connections'); ?></h1>
      </div>
      <?php 
        // output the icon widget
        print theme('hybridauth-user-widget', $path, $providers, $connected);
      ?>    
    </div>
  </div>
  <div class="clearBoth"></div>
</div>
<?php
/**
* HybridAuth internal function for loading the configuration into HybridAuth
*/
function hybridauth_config() {
  /**
   * - "base_url" is the url to HybridAuth EndPoint 'index.php'
   * - "providers" is the list of providers supported by HybridAuth
   * - "enabled" can be true or false; if you dont want to use a specific provider then set it to 'false'
   * - "keys" are your application credentials for this provider 
   *     for example :
   *         'id' is your facebook application id
   *         'key' is your twitter application consumer key
   *         'secret' is your twitter application consumer secret 
   * - To enable Logging, set debug_mode to true, then provide a path of a writable file on debug_file
   *  
   * Note: The HybridAuth Config file is not required, to know more please visit:
   *       http://hybridauth.sourceforge.net/userguide/Configuration.html
   */
  global $base_path;
  global $base_root;
  return array( 
    // set on "base_url" the url that point to HybridAuth Endpoint (where the index.php is found)
    "base_url" => $base_root.$base_path.'/hybridauth',  
    "providers" => array(
      "Blogger" => array( 
        "enabled" => variable_get('hybridauth_blogger_enabled', 0),
      ),
      "WordPress" => array( 
        "enabled" => variable_get('hybridauth_wordpress_enabled', 0),
      ),
      "LiveJournal" => array( 
        "enabled" => variable_get('hybridauth_livejournal_enabled', 0),
      ),
      "OpenID" => array( 
        "enabled" => variable_get('hybridauth_openid_enabled', 0), // no keys required for OpenID based providers
      ),
      "Google" => array( 
        "enabled" => variable_get('hybridauth_google_enabled', 0),
      ),
      "Yahoo" => array( 
        "enabled" => variable_get('hybridauth_yahoo_enabled', 0), 
      ),
      "AOL" => array( // 'devid' is your AOL Developer ID, try "bs1p7S-EREVcBHk3" it may work
        "enabled" => variable_get('hybridauth_aol_enabled', 0),
        "keys" => array( "devid" => variable_get('hybridauth_aol_key', 0) ) 
      ),
      "Facebook" => array( // 'id' is your facebook application id
        "enabled" => variable_get('hybridauth_facebook_enabled', 0),
        "keys" => array( "id" => variable_get('hybridauth_facebook_key', 0), "secret" => variable_get('hybridauth_facebook_secret', 0) ) 
      ),
      "MySpace"  => array(
        "enabled" => variable_get('hybridauth_myspace_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_myspace_key', 0), "secret" => variable_get('hybridauth_myspace_secret', 0) )
      ),
      "Twitter" => array( // 'key' is your twitter application consumer key
        "enabled" => variable_get('hybridauth_twitter_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_twitter_key', 0), "secret" => variable_get('hybridauth_twitter_secret', 0) )
      ),
      "Live"  => array( 
        "enabled" => variable_get('hybridauth_live_enabled', 0),
        "keys" => array( "id" => variable_get('hybridauth_live_key', 0), "secret" => variable_get('hybridauth_live_secret', 0) ) 
      ),
      "LinkedIn" => array( 
        "enabled" => variable_get('hybridauth_linkedin_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_linkedin_key', 0), "secret" => variable_get('hybridauth_linkedin_secret', 0) )
      ),
      "Viadeo" => array( 
        "enabled" => variable_get('hybridauth_viadeo_enabled', 0),
        "keys" => array( "id" => variable_get('hybridauth_viadeo_key', 0), "secret" => variable_get('hybridauth_viadeo_secret', 0) )
      ),
      "Tumblr"   => array( 
        "enabled" => variable_get('hybridauth_tumblr_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_tumblr_key', 0), "secret" => variable_get('hybridauth_tumblr_secret', 0) )
      ),
      "Identica" => array( 
        "enabled" => variable_get('hybridauth_identica_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_identica_key', 0), "secret" => variable_get('hybridauth_identica_secret', 0) )
      ),
      "Vimeo" => array( 
        "enabled" => variable_get('hybridauth_vimeo_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_vimeo_key', 0), "secret" => variable_get('hybridauth_vimeo_secret', 0) )
      ),
      "Foursquare" => array( 
        "enabled" => variable_get('hybridauth_foursquare_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_foursquare_key', 0), "secret" => variable_get('hybridauth_foursquare_secret', 0) )
      ),
      "LastFM"   => array( 
        "enabled" => variable_get('hybridauth_lastfm_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_lastfm_key', 0), "secret" => variable_get('hybridauth_lastfm_secret', 0) )
      ),
      "Gowalla"  => array( 
        "enabled" => variable_get('hybridauth_gowalla_enabled', 0),
        "keys" => array( "key" => variable_get('hybridauth_gowalla_key', 0), "secret" => variable_get('hybridauth_gowalla_secret', 0) )
      ),
      "PayPal"  => array( 
        "enabled" => variable_get('hybridauth_paypal_enabled', 0), 
      ),
      "Flickr"  => array( 
        "enabled" => variable_get('hybridauth_flickr_enabled', 0), 
      ),
    ),
    
    // if you want to enable logging, set 'debug_mode' to true  then provide here a writable file by the web server 
    "debug_mode"   => false,
    
    "debug_file"   => "", 
  );
}

/**
* HybridAuth End Point inclusion
*/
function hybridauth_setup() {

  // ------------------------------------------------------------------------
  //  HybridAuth End Point
  // ------------------------------------------------------------------------
  
    /**
     * here we implement some needed stuff for OpenID
     * - policy
     * - Relying Party Discovery for OpenID
     *
     *  # http://blog.nerdbank.net/2008/06/why-yahoo-says-your-openid-site.html
     *     You must advertise your XRDS document from your Realm URL
     *     Add the following line inside the HEAD tags of your Realm page 
     *
     *  # http://developer.yahoo.com/openid/faq.html
     *     Yahoo! displays the above warning for Relying Parties which fail to 
     *     implement Section 13: Discovering OpenID Relying Parties of the 
     *     OpenID 2.0 Protocol. Implementing Relying Party Discovery enables 
     *     Yahoo to verify your site's OpenID Realm when 
     *     servicing Authentication Requests from your site. 
     * 
     * @author     Zachy <hybridauth@gmail.com>
     * @see        Section 13: Discovering OpenID Relying Parties of the OpenID 2.0 Protocol
     * @link       http://wiki.openid.net
     * @link       http://developer.yahoo.com/openid/faq.html
     * @link       http://blog.nerdbank.net/2008/06/why-yahoo-says-your-openid-site.html
     */
  
  // ------------------------------------------------------------------------
  $output = '';
  $filepath = dirname(__FILE__);
  
  require_once($filepath."/hybridauth/Hybrid/Auth.php" );
  
  # if windows_live_channel requested, we return our windows_live WRAP_CHANNEL_URL
  if( isset( $_REQUEST["get"] ) && $_REQUEST["get"] == "windows_live_channel" )
  {
    $output .= 
      file_get_contents($filepath."/hybridauth/Hybrid/resources/windows_live_channel.html" ); 

    exit( 0 );
  }

  # if openid_policy requested, we return our policy document  
  if( isset( $_REQUEST["get"] ) && $_REQUEST["get"] == "openid_policy")
  {
    $output .= 
      file_get_contents($filepath."/hybridauth/Hybrid/resources/openid_policy.html" ); 

    exit( 0 );
  }

  # if openid_xrds requested, we return our XRDS document 
  if( isset( $_REQUEST["get"] ) && $_REQUEST["get"] == "openid_xrds" )
  {
    header("Content-Type: application/xrds+xml");

    $output .= str_replace
      (
        "{RETURN_TO_URL}", 
        Hybrid_Auth::getCurrentUrl( false ) ,
        file_get_contents($filepath."/hybridauth/Hybrid/resources/openid_xrds.xml" )
      ); 

    exit( 0 );
  } 

  # if we get a hauth.start or hauth.done
  if( isset( $_REQUEST["hauth_start"] ) || isset( $_REQUEST["hauth_done"] ) )
  {
    # init Hybrid_Auth
    try{ 
      // check if Hybrid_Auth session already exist
      if( ! isset( $_SESSION["HA::CONFIG"] ) ): 
        header("HTTP/1.0 404 Not Found");

        die( "Sorry, this page cannot be accessed directly!" );
      endif; 

      Hybrid_Auth::initialize( unserialize( $_SESSION["HA::CONFIG"] ) ); 
    }
    catch( Exception $e )
    { 
      Hybrid_Logger::error( "Endpoint: Error while trying to init Hybrid_Auth" ); 

      header("HTTP/1.0 404 Not Found");

      die( "Oophs. Error!" );
    }

    Hybrid_Logger::info( "Enter Endpoint" ); 

    # define:endpoint step 3.
    # yeah, why not a switch!
    if( isset( $_REQUEST["hauth_start"] ) && $_REQUEST["hauth_start"] )
    {
      $provider_id = trim( strip_tags( $_REQUEST["hauth_start"] ) );

      # check if page accessed directly
      if( ! Hybrid_Auth::storage()->get( "hauth_session.$provider_id.hauth_endpoint" ) )
      {
        Hybrid_Logger::error( "Endpoint: hauth_endpoint parameter is not defined on hauth_start, halt login process!" );

        header("HTTP/1.0 404 Not Found");

        die( "Sorry, this page cannot be accessed directly!" );
      }

      # define:hybrid.endpoint.php step 2.
      $hauth = Hybrid_Auth::setup( $provider_id );

      # if REQUESTed hauth_idprovider is wrong, session not created, or shit happen, etc. 
      if( ! $hauth )
      {
        Hybrid_Logger::error( "Endpoint: Invalide parameter on hauth_start!" ); 
        
        header("HTTP/1.0 404 Not Found");

        die( "Invalide parameter! Please return to the login page and try again." );
      }

      try
      { 
        Hybrid_Logger::info( "Endpoint: call adapter [{$provider_id}] loginBegin()" );
        
        $hauth->adapter->loginBegin();
      }
      catch( Exception $e )
      {
        Hybrid_Logger::error( "Exception:" . $e->getMessage(), $e );

        Hybrid_Error::setError( $e->getMessage(), $e->getCode(), $e->getTraceAsString(), $e );

        $hauth->returnToCallbackUrl();
      }

      die();
    }

    # define:endpoint step 3.1 and 3.2
    if( isset( $_REQUEST["hauth_done"] ) && $_REQUEST["hauth_done"] ) 
    {
      $provider_id = trim( strip_tags( $_REQUEST["hauth_done"] ) );

      $hauth = Hybrid_Auth::setup( $provider_id );
      
      if( ! $hauth )
      {
        Hybrid_Logger::error( "Endpoint: Invalide parameter on hauth_done!" ); 

        $hauth->adapter->setUserUnconnected();

        header("HTTP/1.0 404 Not Found"); 

        die( "Invalide parameter! Please return to the login page and try again." );
      }

      try
      {
        Hybrid_Logger::info( "Endpoint: call adapter [{$provider_id}] loginFinish() " );
        
        $hauth->adapter->loginFinish(); 
      }
      catch( Exception $e )
      {
        Hybrid_Logger::error( "Exception:" . $e->getMessage(), $e );

        Hybrid_Error::setError( $e->getMessage(), $e->getCode(), $e->getTraceAsString(), $e );

        $hauth->adapter->setUserUnconnected(); 
      }

      Hybrid_Logger::info( "Endpoint: job done. retrun to callback url." );

      $hauth->returnToCallbackUrl();

      die();
    }
  }
  else{
    # Else, 
    # We advertise our XRDS document, something supposed to be done from the Realm URL page 
    $output .= str_replace
      (
        "{X_XRDS_LOCATION}",
        Hybrid_Auth::getCurrentUrl( false ) . "?get=openid_xrds",
        file_get_contents($filepath."/hybridauth/Hybrid/resources/openid_realm.html" )
      ); 
  }
  return $output;
}
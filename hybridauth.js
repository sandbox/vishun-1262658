$(function() {

  // setup some variables
  var rel = null;
  var popups = Drupal.settings.hybridauth.popups;
  if (typeof popups != 'undefined') {
    if (popups == 0) {
      popups = 'no';
    } else if (popups == 1) {
      popups = 'yes';
    }
  } else {
    popups = '';
  }
  
  function start_auth( params ){
    start_url = params + "&return_to=" + "&_ts=" + (new Date()).getTime();
    if (popups == 'yes') {
      window.open(
        start_url, 
        "hybridauth_social_sing_on", 
        "location=0,status=0,scrollbars=1,width=800,height=500"
      );      
    } else {
      window.location = start_url;
    }
  }
  
  $(".provider .icon").click(
    function(){ 
      rel = $( this ).attr( "rel" );
      switch( rel ){
        case "aol":
        case "facebook":
        case "foursquare":
        case "google":
        case "gowalla":
        case "lastfm":
        case "linkedin":
        case "live":
        case "myspace":
        case "paypal":
        case "tumblr":
        case "twitter":
        case "vimeo":
        case "yahoo":
                start_auth( "/hybridauth/"+rel+"?popup="+popups);
                break; 
        case "wordpress" : case "blogger" : case "flickr" :  case "livejournal" :  
                if( rel == "blogger" ){
                  $("#openidm").html( "Please enter your blog name" );
                }
                else{
                  $("#openidm").html( "Please enter your username" );
                }
                $("#openidun").css( "width", "220" );
                $("#openidimg").attr( "src", "images/icons/" + rel + ".png" );
                $("#hybridauth_widget #providers, .hybridauth_header").hide();
                $("#openidid").show();  
                break;
        case "openid" : 
                $("#openidm").html( "Please enter your OpenID URL" );
                $("#openidun").css( "width", "350" );
                $("#openidimg").attr( "src", "images/icons/" + rel + ".png" );
                $("#hybridauth_widget #providers, .hybridauth_header").hide();
                $("#openidid").show();  
                break;

        //default: alert( "u no fun" );
      }
    }
  ); 

  $("#openidbtn").click(
    function(){
      un = $( "#openidun" ).val();

      if( ! un ){
        alert( "Please provider your user name." );
        
        return false;
      }

      switch( rel ){ 
        case "wordpress" : oi = "http://" + un + ".wordpress.com"; break;
        case "livejournal" : oi = "http://" + un + ".livejournal.com"; break;
        case "blogger" : oi = "http://" + un + ".blogspot.com"; break;
        case "flickr" : oi = "http://www.flickr.com/photos/" + un + "/"; break;   
      }
      
      start_auth( "?provider=OpenID&openid_identifier=" + escape( oi ) ); 
    }
  );  

  $("#backtolist").click(
    function(){
      $("#hybridauth_widget #providers, .hybridauth_header").show();
      $("#openidid").hide();

      return false;
    }
  );  
});
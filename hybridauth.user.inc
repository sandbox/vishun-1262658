<?php
/**
* User account -> Authentication -> Dashboard
*/
function hybridauth_user_dashboard() {
      
  // check if user is connected to a provider
/*  if ($providers = $hybridauth->getConnectedProviders()) {
    global $user;
    if (!$user->uid) {
      $provider = $providers[0];
      $account = $hybridauth->authenticate($provider);
      $profile = $account->getUserProfile();
      $username = str_replace(' ','',$profile->displayName);
      // check if username already exists
      //if ($result = db_result(db_query("SELECT name FROM {users} WHERE name='%s'",$username))) {
        // add a random number onto the end
      //  $username .= rand(1000,9999);          
      //}
      user_external_login_register($username, 'HybridAuth');    
    }
  } */
  //$account = $hybridauth->authenticate('twitter');
  //$is_user_logged_in = $account->isUserConnected();
  //$output .= print_r($account->getUserProfile(),true);
  //$output .= print_r($_SESSION,true);
  
  $hybridauth = hybridauth_api();
  
  $path = drupal_get_path('module', 'hybridauth');
  $providers = hybridauth_providers('enabled');
  $connected = $hybridauth->getConnectedProviders();
  
  // determine whether or not to work with popups
  $popups = variable_get('hybridauth_popups', 0);
  if ($popup == 0) {
    $popup = 'no';
  } else if ($popup == 1) {
    $popup = 'yes';
  }
  
  // add dynamic variables for dashboard template
  $vars = array(
    'popups' => $popups,
  );
  drupal_add_js(array('hybridauth' => $vars), 'setting');
  
  drupal_add_css($path.'/hybridauth.css');
  drupal_add_js($path.'/hybridauth.js');
  $output .= theme('hybridauth-user-dashboard', $path, $providers, $connected);
  
  return $output;
}

/**
* User account -> Authentication -> Dashboard
*/
function hybridauth_user_settings($provider) {
  $path = drupal_get_path('module', 'hybridauth'); 
  drupal_add_css($path.'/hybridauth.css');
  drupal_add_js($path.'/hybridauth.js');
  $account = hybridauth_api('get', $provider);
  $profile = $account->getUserProfile();
  if ($provider == 'twitter') {
    $followers = $account->api()->get('followers/ids')->ids;
    $content['followers'] = $account->api()->get('users/lookup', array('user_id' => implode(',', $followers)));
    $friends = $account->api()->get('friends/ids')->ids;
    $content['friends'] = $account->api()->get('users/lookup', array('user_id' => implode(',', $friends)));
    
    // perform befriending
    if (variable_get('hybridauth_befriend_profile', 0) == true && variable_get('hybridauth_befriend_twitter_enabled', 0) == true) {
      $sitetwitter = variable_get('hybridauth_befriend_twitter_screen_name', null);
      // check if befriend user is already a friend
      $content['befriend'] = $account->api()->get('friendships/show', array('source_id' => $profile->identifier, 'target_screen_name' => $sitetwitter));
    
      // they're not a friend, suggest becoming a friend
      if (!$content['befriend']->relationship->source->following) {
        //print_r($account->api()->post('friendships/create', array('screen_name' => $sitetwitter)));
      } else {
        $output .= 'Thanks for following us!';
      }
    }
  }
  //$output['status_update'] = $account->api()->post('statuses/update', array('status' => 'yay'));
  $output .= theme('hybridauth-user-settings', $provider, $profile, $content);
  return $output;
}

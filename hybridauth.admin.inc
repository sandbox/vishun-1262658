<?php
/**
* HybridAuth general settings page
*/
function hybridauth_admin_general() {
  $form = array();
  
  $form['config'] = array(
    '#type' => 'fieldset',
    '#title' => null,
    '#description' => 'HybridAuth is an Open Source Social Sign On Solution PHP Library for authentication through identity providers like Facebook, Twitter, Google, Yahoo, LinkedIn, MySpace, Windows Live, OpenID, Foursquare, AOL and others. '.l('hybridauth.sourceforge.net', 'http://hybridauth.sourceforge.net', array('attributes'=>array('target'=>'_blank'))),
  );
  
  // general settings
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Settings',
    '#description' => t('Settings for generally how things will work and function.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['registration'] = array(
    '#type' => 'radios',
    '#title' => t('Registration style'),
    '#description' => t('This determines what happens when a user authenticates for the first time and they do not have a Drupal user account. The normal operation is to allow them to customize their username and fill specify their email address if its missing. Email method will only interrupt the user registration if the email address is missing. Quick will not interrupt the user at all and register them immediately, though this does result in no email address being stored for the user.'),
    '#default_value' => variable_get('hybridauth_registration', 'normal'),
    '#options' => array(
      'normal' => t('Normal: Ask about customizing username and entering email address (if its missing)'), 
      'email' => t('Email: Ask only about email (if its missing)'),
      'quick' => t('Quick: Immediate login/registration (email will be missing)'),
    ),
  );
  $form['settings']['picture'] = array(
    '#type' => 'radios',
    '#title' => t('Download and attach 3rd party avatar to drupal user'),
    '#default_value' => variable_get('hybridauth_login_picture', 1),
    '#options' => array(t('Never'), t('Every login')),
  );
  $form['settings']['login_block'] = array(
    '#type' => 'radios',
    '#title' => t('Append HybridAuth widget to login block'),
    '#default_value' => variable_get('hybridauth_login_block', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['settings']['popups'] = array(
    '#type' => 'radios',
    '#title' => t('Use a popup window for authentication'),
    '#default_value' => variable_get('hybridauth_popups', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  
  // redirect login / register pages
  $form['redirect'] = array(
    '#type' => 'fieldset',
    '#title' => 'Redirection',
    '#description' => t('This will make it so that the normal User Login and User Register pages automatically redirect to the HybridAuth Login and Registration pages. These pages have the same form plus the option to Login or Register using the enabled HybridAuth providers.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['redirect']['login'] = array(
    '#type' => 'radios',
    '#title' => t('Redirect Login Page'),
    '#default_value' => variable_get('hybridauth_redirect_login', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['redirect']['register'] = array(
    '#type' => 'radios',
    '#title' => t('Redirect Register Page'),
    '#default_value' => variable_get('hybridauth_redirect_register', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  
  return system_settings_form($form); 
}

/**
* HybridAuth settings page validation
*/
function hybridauth_admin_general_validate($form, &$form_state) {
  variable_set('hybridauth_registration', $form_state['values']['settings']['registration']);
  variable_set('hybridauth_login_picture', $form_state['values']['settings']['login_picture']);
  variable_set('hybridauth_login_block', $form_state['values']['settings']['login_block']);
  variable_set('hybridauth_popups', $form_state['values']['settings']['popups']);
  variable_set('hybridauth_redirect_login', $form_state['values']['redirect']['login']);
  variable_set('hybridauth_redirect_register', $form_state['values']['redirect']['register']);
}
  
/**
* HybridAuth providers settings page
*/
function hybridauth_admin_providers() {
  $form = array();
  
  // Enabled providers and API credentials
  $providers = hybridauth_providers();
  foreach ($providers as $provider) {
  
    // Check if the provider is Enabled
    // That way we can default it to not be collapsed
    $enabled = variable_get('hybridauth_'.$provider.'_enabled', 0);
    if ($enabled) {
      $collapsed = FALSE; 
    } else {
      $collapsed = TRUE;
    }
    
    $form[$provider] = array(
      '#type' => 'fieldset',
      '#title' => t(ucfirst($provider)),
      '#description' => '<p>'.l('Setup Instructions', 'http://hybridauth.sourceforge.net/userguide/IDProvider_info_'.ucfirst($provider).'.html', array('attributes'=>array('target'=>'_blank'))).'</p>',
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
    );
    $form[$provider]['enabled'] = array(
      '#type' => 'radios',
      '#title' => t('Status'),
      '#default_value' => $enabled,
      '#options' => array(t('Disabled'), t('Enabled')),
    );
    $form[$provider]['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Key'),
      '#default_value' => variable_get('hybridauth_'.$provider.'_key', null),
    );
    $form[$provider]['secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Secret'),
      '#default_value' => variable_get('hybridauth_'.$provider.'_secret', null),
    );
  }
  
  // fixes
  $form['aol']['key']['#title'] = t('Dev ID');
  $form['aol']['#description'] = '<p>'.l('Setup Instructions', 'http://hybridauth.sourceforge.net/userguide/IDProvider_info_AOL.html', array('attributes'=>array('target'=>'_blank'))).'</p>';  
  unset($form['aol']['secret']);
  unset($form['flickr']['key']);
  unset($form['flickr']['secret']);
  unset($form['flickr']['#description']);
  unset($form['blogger']['key']);
  unset($form['blogger']['secret']);
  unset($form['blogger']['#description']);
  unset($form['wordpress']['key']);
  unset($form['wordpress']['secret']);
  unset($form['wordpress']['#description']);
  unset($form['livejournal']['key']);
  unset($form['livejournal']['secret']);
  unset($form['livejournal']['#description']);
  unset($form['google']['key']);
  unset($form['google']['secret']);
  unset($form['yahoo']['key']);
  unset($form['yahoo']['secret']);
  $form['openid']['#description'] = '<p>'.l('Setup Instructions', 'http://hybridauth.sourceforge.net/userguide/IDProvider_info_OpenID.html', array('attributes'=>array('target'=>'_blank'))).'</p>';
  unset($form['openid']['key']);
  unset($form['openid']['secret']);
  $form['paypal']['#description'] = '<p>'.l('Setup Instructions', 'http://hybridauth.sourceforge.net/userguide/IDProvider_info_PayPal.html', array('attributes'=>array('target'=>'_blank'))).'</p>';
  unset($form['paypal']['key']);
  unset($form['paypal']['secret']);
  $form['live']['key']['#title'] = t('ID');
  $form['viadeo']['key']['#title'] = t('ID');
  $form['twitter']['#description'] .= '<p>'.l('Twitter API methods', 'https://dev.twitter.com/docs/api', array('attributes'=>array('target'=>'_blank'))).'</p>';
  $form['twitter']['key']['#description'] = 'Twitter Application Consumer Key';
  $form['facebook']['key']['#title'] = t('ID');
  $form['facebook']['key']['#description'] = 'Facebook Application ID';
  $form['myspace']['#description'] = '<p>'.l('Setup Instructions', 'http://hybridauth.sourceforge.net/userguide/IDProvider_info_MySpace.html', array('attributes'=>array('target'=>'_blank'))).'</p>';
  $form['linkedin']['#description'] = '<p>'.l('Setup Instructions', 'http://hybridauth.sourceforge.net/userguide/IDProvider_info_LinkedIn.html', array('attributes'=>array('target'=>'_blank'))).'</p>';
  $form['lastfm']['#description'] = l('Setup Instructions', 'http://hybridauth.sourceforge.net/userguide/IDProvider_info_LastFM.html', array('attributes'=>array('target'=>'_blank')));
  
  return system_settings_form($form);
}

/**
* HybridAuth settings page validation
*/
function hybridauth_admin_providers_validate($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    if (in_array($key, hybridauth_providers())) {
      if ($value['enabled'] == true) {
        if (isset($value['key']) && isset($value['secret']) && !$value['key'] && !$value['secret']) {
          form_set_error($key, 'To enable '.ucfirst($key).' you must provide the API Key/ID and Secret');
          return false;
        } else if (isset($value['key']) && !$value['key']) {
          form_set_error($key.'][key', 'To enable '.$key.' you must provide the API Key or ID');
          return false;
        } else if (isset($value['secret']) && !$value['secret']) {
          form_set_error($key.'][secret', 'To enable '.$key.' you must provide the API Secret');
          return false;
        }
        variable_set('hybridauth_'.$key.'_enabled', $value['enabled']);
      } else {
        variable_set('hybridauth_'.$key.'_enabled', $value['enabled']);
      }
      variable_set('hybridauth_'.$key.'_key', $value['key']);
      variable_set('hybridauth_'.$key.'_secret', $value['secret']);
    }
  }
}

/**
* HybridAuth providers sorting
*/
function hybridauth_admin_providers_sort($form_state) {

  // this designates that the elements in my_items are a collection
  // doing this avoids the "id hack" in the Computer Minds article
  $form['my_items']['#tree'] = TRUE;

  // fetch the data from the DB
  // we're just pulling 3 fields for each record, for example purposes
  $result = db_query("SELECT pid, provider, weight FROM {hybridauth_providers} ORDER BY weight ASC, provider ASC");

  // iterate through each result from the DB
  while ($item = db_fetch_object($result)) {
    // my_item will be an array, keyed by DB id, with each element being an array that holds the table row data
    $form['my_items'][$item->pid] = array(
      // "name" will go in our form as regular text (it could be a textfield if you wanted)
      'name' => array(
        '#type' => 'markup',
        '#value' => $item->provider,
        ),
      // the "weight" field will be manipulated by the drag and drop table
      'weight' => array(
        '#type' => 'weight',
        '#delta' => 10,
        '#default_value' => $item->weight,
        '#attributes' => array('class' => 'weight'),
        ),
      );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
    );

  return $form;
}

/**
* HybridAuth providers sorting theme callback
*/
function theme_hybridauth_admin_providers_sort($form) {
  // the variable that will hold our form HTML output
  $output = '';

  //loop through each "row" in the table array
  foreach($form['my_items'] as $id => $row) {

    // if $id is not a number skip this row in the data structure
    if (!intval($id))
      continue;

    // this array will hold the table cells for a row
    $this_row = array();

    // first, add the "name" markup
    $this_row[] = drupal_render($row['name']);

    // Add the weight field to the row
    // the Javascript to make our table drag and drop will end up hiding this cell
    $this_row[] = drupal_render($row['weight']);

    //Add the row to the array of rows
    $table_rows[] = array('data' => $this_row, 'class'=>'draggable');
  }

  // Make sure the header count matches the column count
  $header = array(
    'Provider',
    'Weight',
    );

  $table_id = 'my_items';

  // this function is what brings in the javascript to make our table drag-and-droppable
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'weight');   

  // over-write the 'my_items' form element with the markup generated from our table
  $form['my_items'] = array(
    '#type' => 'markup',
    '#value' => theme('table', $header, $table_rows, array('id' => $table_id)),
    '#weight' => '1',
    );

  // render the form
  // note, this approach to theming the form allows you to add other elements in the method
  // that generates the form, and they will automatically be rendered correctly
  $output = drupal_render($form);

  return $output;
}

/**
* HybridAuth providers sorting submit handler to save the weight
*/
function hybridauth_admin_providers_sort_submit($form, &$form_state) {
  // just iterate through the submitted values
  // because we keyed the array with DB ids, it's pretty simple
  foreach ($form_state['values']['my_items'] as $id => $item) {
    db_query("UPDATE {hybridauth_providers} SET weight=%d WHERE pid=%d",$item['weight'], $id);
  }
}

/**
* HybridAuth friends settings page
*/
function hybridauth_admin_friends() {
  $form = array();

  // friend finder
  $form['finder'] = array(
    '#type' => 'fieldset',
    '#title' => 'Friend Finder',
    '#description' => t('Help users find friends on your service. This grabs the current users friend list (where available) and attempts to locate those friends in the database. This is not available yet, but it will likely attempt to work with the User Relationships and Flag module.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['finder']['enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Show Friend Finder after initial authentication'),
    '#default_value' => variable_get('hybridauth_finder_enabled', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#disabled' => TRUE,
  );
  
  // befriending
  $form['befriend'] = array(
    '#type' => 'fieldset',
    '#title' => 'Befriending',
    '#description' => t('Check if the current user is presently a friend or follower and suggest they become one (if they are not already). This will attempt to create the friendship automatically where applicable. For example, after user approval, the current user will start following the specified Twitter account.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['befriend']['profile'] = array(
    '#type' => 'radios',
    '#title' => t('Suggest befriending on profile settings'),
    '#default_value' => variable_get('hybridauth_befriend_profile', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['befriend']['auth'] = array(
    '#type' => 'radios',
    '#title' => t('Suggest befriending after authentication'),
    '#default_value' => variable_get('hybridauth_befriend_auth', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['befriend']['ask'] = array(
    '#type' => 'radios',
    '#title' => t('Suggestion frequency'),
    '#default_value' => variable_get('hybridauth_befriend_ask', 0),
    '#options' => array(t('Only once after initial authentication'), t('Every authentication (may annoy users)')),
  );
  $form['befriend']['twitter'] = array(
    '#type' => 'fieldset',
    '#title' => 'Twitter',
    '#description' => t('This will allow you to gain more friends and followers by suggesting that authenticated users become a friend (if they are not one already)'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['befriend']['twitter']['enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Twitter Befriending'),
    '#default_value' => variable_get('hybridauth_befriend_twitter_enabled', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['befriend']['twitter']['screen_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter Screen name'),
    '#description' => t('You do not need to include the @ symbol'),
    '#default_value' => variable_get('hybridauth_befriend_twitter_screen_name', null),
  );

  return system_settings_form($form); 
}

/**
* HybridAuth friends settings validation
*/
function hybridauth_admin_friends_validate($form, &$form_state) {
  variable_set('hybridauth_befriend_ask', $form_state['values']['befriend']['ask']);
  variable_set('hybridauth_befriend_auth', $form_state['values']['befriend']['auth']);
  variable_set('hybridauth_befriend_profile', $form_state['values']['befriend']['profile']);
  variable_set('hybridauth_befriend_twitter_enabled', $form_state['values']['befriend']['twitter']['enabled']);
  variable_set('hybridauth_befriend_twitter_screen_name', $form_state['values']['befriend']['twitter']['screen_name']);
  variable_set('hybridauth_finder_enabled', $form_state['values']['finder']['enabled']);
}